<!DOCTYPE html>
<html lang="en">
<head>
  <title>Betfair</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/modules/exporting.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 940px}

    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }

    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }

    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;}
    }
  </style>
</head>
<body>
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>List of matches:</h4>
      <ul class="nav nav-pills nav-stacked">
         @include('layouts.sidebar')
      </ul>
    </div>

    <div id="content" class="col-sm-9">
      @yield('content')
    </div>
  </div>
</div>
<footer class="container-fluid">
  <p>Betfair company ltd.</p>
</footer>

  <script>
    $(document).ready(function(){
      $('.nav a').on('click',function(){
          event.preventDefault();

          var url = $(this).html();


          $(".nav li").each(function(){
                $(this).removeClass('active');
          })

          $(this).parent().addClass('active');

          $.get(url,{pair : url},function(data){
            $('#content').html(data);
          });
      });
    });
  </script>
</body>
</html>
