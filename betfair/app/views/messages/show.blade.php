<h1><medium>Statistic information about match: {{ $pair }}</medium></h1>
    <div class="col-sm-6">
      <hr>
     <div id="percentPie" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
    </div>
    <div class="col-sm-6">
      <hr>
      <div id="amountGraph" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
    </div>

    <script type="text/javascript">
      $(document).ready(function() {

        var percentPieOptions = {
          chart: {
            renderTo: 'percentPie',
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
          },
          title: {
            text: 'Percent of bets from different countries'
          },
          tooltip: {
            formatter: function() {
              return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
            }
          },
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                enabled: true,
                color: '#000000',
                connectorColor: '#000000',
                formatter: function() {
                  return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %';
                }
              }
            }
          },
          series: [{
            type: 'pie',
            name: 'Browser share',
            data: []
          }]
        };


        var amountGraphOptions = {
          chart: {
                type: 'column',
                renderTo: 'amountGraph',
            },
            title: {
                text: 'Amount of bets from diffent countries:'
            },

            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'USD'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Amount: <b>$ {point.y:.1f}</b>'
            },
            series: [{
                name: 'Match',
                data: [],
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        };
        $.getJSON("http://localhost/restful/silex/web/index.php/api/messages/{{ $pair }}", function(json) {
          percentPieOptions.series[0].data = json["percent"];
          amountGraphOptions.series[0].data = json["amount"];
          percentPie = new Highcharts.Chart(percentPieOptions);
          amountGraph = new Highcharts.Chart(amountGraphOptions);
        });
      });


    </script>
