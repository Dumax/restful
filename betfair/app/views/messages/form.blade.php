@extends('layouts.default')

@section('content')

    {{  Form::open(array('url' => 'http://localhost:8080/api/messages','method' => 'get')) }}
    <h1>Bet information</h1>
    <div class="form-group">
        {{ Form::label('userId','UserId:',array('for' => 'userId')) }}
        {{ Form::text('userId', null, array('class' => 'form-control','id' => 'userId')) }}
    </div>
    <div class="form-group">
        <label for="countryHome">Country Home:</label>
        <select class="form-control" name="countryHome" id="countryHome">
            @foreach ($countries as $country)
                <option>{{ $country->country_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="countryAway">Country Away:</label>
        <select class="form-control" name="countryAway" id="countryAway">
            @foreach ($countries as $country)
                <option>{{ $country->country_name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        {{ Form::label('betAmount','Bet Amount:',array('for' => 'betAmount', 'id' => 'betAmount')) }}
        {{ Form::text('betAmount', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('coefficient','Coefficient:',array('for' => 'coefficient', 'id' => 'coefficient')) }}
        {{ Form::text('coefficient', null, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        <label for="recipientFrom">Recipient From:</label>
        <select class="form-control" name="recipientFrom" id="recipientFrom">
            @foreach ($countries as $country)
                <option>{{ $country->country_code }}</option>
            @endforeach
        </select>
    </div>
    {{ Form::submit('Make bet', array('class' => 'btn btn-success')) }}
    {{ Form::close() }}
@stop