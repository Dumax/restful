-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 25 2016 г., 09:56
-- Версия сервера: 5.6.27-0ubuntu1
-- Версия PHP: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `betfair`
--

-- --------------------------------------------------------

--
-- Структура таблицы `betmessages`
--

CREATE TABLE IF NOT EXISTS `betmessages` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `countryHome` varchar(255) NOT NULL,
  `countryAway` varchar(255) NOT NULL,
  `betAmount` int(11) NOT NULL,
  `coefficient` decimal(10,2) NOT NULL,
  `timePlaced` varchar(255) NOT NULL,
  `recipientFrom` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `betmessages`
--

INSERT INTO `betmessages` (`id`, `userId`, `countryHome`, `countryAway`, `betAmount`, `coefficient`, `timePlaced`, `recipientFrom`, `created_at`, `updated_at`) VALUES
(1, 134256, 'Ukraine', 'England', 32, 0.88, '24-AUG-15 10:27:44', 'GB', '2016-01-24 13:41:19', '2016-01-24 13:41:19'),
(2, 134256, 'Ukraine', 'England', 32, 0.88, '24-AUG-15 10:27:44', 'PL', '2016-01-24 13:41:26', '2016-01-24 13:41:26'),
(3, 134256, 'Ukraine', 'England', 24, 0.88, '24-AUG-15 10:27:44', 'US', '2016-01-24 14:11:29', '2016-01-24 14:11:29'),
(4, 134256, 'Ukraine', 'England', 114, 0.88, '24-AUG-15 10:27:44', 'IT', '2016-01-24 14:45:31', '2016-01-24 14:45:31'),
(5, 134256, 'Spain', 'Portugal', 24, 0.88, '24-AUG-15 10:27:44', 'UA', '2016-01-24 14:48:04', '2016-01-24 14:48:04'),
(6, 134256, 'Spain', 'Portugal', 34, 0.88, '24-AUG-15 10:27:44', 'PL', '2016-01-24 14:48:10', '2016-01-24 14:48:10'),
(7, 134256, 'Spain', 'Portugal', 54, 0.88, '24-AUG-15 10:27:44', 'AU', '2016-01-24 14:48:19', '2016-01-24 14:48:19'),
(8, 134256, 'Spain', 'Portugal', 24, 0.88, '24-AUG-15 10:27:44', 'AU', '2016-01-24 14:48:22', '2016-01-24 14:48:22'),
(9, 134256, 'Spain', 'Portugal', 21, 0.88, '24-AUG-15 10:27:44', 'GB', '2016-01-24 14:48:36', '2016-01-24 14:48:36'),
(10, 134256, 'Spain', 'Portugal', 111, 0.88, '24-AUG-15 10:27:44', 'US', '2016-01-24 14:53:00', '2016-01-24 14:53:00'),
(11, 134256, 'Spain', 'Portugal', 11, 0.88, '24-AUG-15 10:27:44', 'RU', '2016-01-24 14:53:21', '2016-01-24 14:53:21'),
(12, 134256, 'Spain', 'Italy', 34, 0.88, '24-AUG-15 10:27:44', 'UA', '2016-01-24 14:56:34', '2016-01-24 14:56:34'),
(13, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'RU', '2016-01-24 14:56:59', '2016-01-24 14:56:59'),
(14, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'RU', '2016-01-24 16:39:59', '2016-01-24 16:39:59'),
(15, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'PO', '2016-01-24 16:41:40', '2016-01-24 16:41:40'),
(16, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'PO', '2016-01-24 16:43:44', '2016-01-24 16:43:44'),
(17, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'PO', '2016-01-24 16:44:20', '2016-01-24 16:44:20'),
(18, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'PO', '2016-01-24 16:46:48', '2016-01-24 16:46:48'),
(19, 134256, 'Spain', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'PO', '2016-01-24 18:26:39', '2016-01-24 18:26:39'),
(20, 222, 'Russia', 'Italy', 22, 0.88, '24-AUG-15 10:27:44', 'FR', '2016-01-24 18:33:23', '2016-01-24 18:33:23'),
(21, 222, 'Russia', 'Italy', 32, 0.88, '24-AUG-15 10:27:44', 'CH', '2016-01-24 20:55:08', '2016-01-24 20:55:08'),
(22, 222, 'Russia', 'Italy', 32, 0.88, '24-AUG-15 10:27:44', 'CH', '2016-01-24 21:11:18', '2016-01-24 21:11:18'),
(23, 222, 'Russia', 'Italy', 32, 0.88, '24-AUG-15 10:27:44', 'CH', '2016-01-24 21:15:00', '2016-01-24 21:15:00');

-- --------------------------------------------------------

--
-- Структура таблицы `phinxlog`
--

CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `start_time`, `end_time`) VALUES
(20160122132835, '2016-01-24 13:41:12', '2016-01-24 13:41:13');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `betmessages`
--
ALTER TABLE `betmessages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `phinxlog`
--
ALTER TABLE `phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `betmessages`
--
ALTER TABLE `betmessages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
