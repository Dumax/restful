<?php

use Phinx\Migration\AbstractMigration;

class TestMigration extends AbstractMigration
{
    public function change()
    {
        // create the table
        $table = $this->table('betmessages');
        $table->addColumn('userId', 'integer')
            ->addColumn('countryHome', 'string')
            ->addColumn('countryAway', 'string')
            ->addColumn('betAmount', 'integer')
            ->addColumn('coefficient', 'decimal', ['precision' => 10, 'scale' => '2'])
            ->addColumn('timePlaced', 'string')
            ->addColumn('recipientFrom', 'string')
            ->addColumn('created_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->addColumn('updated_at', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->create();
    }

    /**
     * Migrate Up.
     */
    public function up()
    {

    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}
