<?php
/**
 * Register Eloquent ORM
 */
$app->register(new \BitolaCo\Silex\CapsuleServiceProvider(), $app['config']['database']);

$app->register(new Silex\Provider\MonologServiceProvider(), $app['config']['log']);
