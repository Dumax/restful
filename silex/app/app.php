<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;


$app = new Silex\Application();

/**
 *
 */
$app['config'] = require_once __DIR__ . '/config/config.php';

/**
 *
 */
require_once 'providers.php';

/**
 *
 */
require_once 'routes.php';

/**
 *
 */
$app['debug'] = $app['config']['app']['debug'];
