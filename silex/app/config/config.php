<?php
$config = [

    'app' => [
        'debug' => true,
    ],

    'database' => [
        'capsule.connection' => [
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'betfair',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => '',
            'logging' => true,
        ],
    ],

    'log' => [
        'monolog.logfile' => __DIR__.'/../log/development.log',
    ],

];

return $config;
