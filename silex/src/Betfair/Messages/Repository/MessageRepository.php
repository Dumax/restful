<?php

namespace Betfair\Messages\Repository;

use Betfair\Messages\Model\Message;
use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

class MessageRepository
{
    protected $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function getListOfMatches(Application $app)
    {
        $data = Message::all();
        $pairs = [];
        foreach ($data as $item) {
            $pair = $item->countryHome . '-' . $item->countryAway;
            if (!in_array($pair, $pairs)) {
                $pairs[] = $pair;
            }
        }

        return $app->json($pairs,200);
    }

    public function saveMessageToDB(Application $app, $data)
    {
        $messages = new Message;
        $messages->userId = $data['userId'];
        $messages->countryHome = $data['countryHome'];
        $messages->countryAway = $data['countryAway'];
        $messages->betAmount = $data['betAmount'];
        $messages->coefficient = $data['coefficient'];
        $messages->timePlaced = $data['timePlaced'];
        $messages->recipientFrom = $data['recipientFrom'];
        $messages->save();

        $app['monolog']->addInfo("Message about match " . $data['countryHome']
            . "-" . $data['countryAway'] . " was saved to database successfully");

        return new Response('Message was saved', Response::HTTP_OK);
    }

    public function getStatisticData($app,$pair)
    {
        $pair = explode("-", $pair);
        $query = Message::where('countryHome', '=', $pair[0])->where('countryAway', '=', $pair[1]);
        $currentPairData = $query->get();
        $sumOfBetsOnPair = $currentPairData->sum('betAmount');

        $statisticData = [];
        $percent = [];
        $amount = [];

        foreach ($currentPairData as $item) {
            $country = $item->recipientFrom;
            $betAmount = $item
                ->where('recipientFrom', $country)
                ->where('countryHome', '=', $pair[0])
                ->where('countryAway', '=', $pair[1])
                ->sum('betAmount');
            $betAmount = intval($betAmount);
            $countryPercentOfBets = $betAmount / $sumOfBetsOnPair;
            $testPercent = [$item->recipientFrom, $countryPercentOfBets];
            if (!in_array($testPercent, $percent)) {
                $percent[] = $testPercent;
            }

            $testAmount = [$item->recipientFrom, $betAmount];
            if (!in_array($testAmount, $amount)) {
                $amount[] = $testAmount;
            }

        }

        $statisticData['percent'] = $percent;
        $statisticData['amount'] = $amount;

        return $app->json($statisticData, 200);

    }
}
