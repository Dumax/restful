<?php
namespace Betfair\Messages\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = "betmessages";
}
