<?php
namespace Betfair\Messages\Controller;

use Betfair\Messages\Model\Message;
use Betfair\Messages\Repository\MessageRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MessagesController
{
    public $repository;
    public $data;

    public function __construct()
    {
        $this->repository = new MessageRepository(new Message);
    }

    public function index(Application $app)
    {
        return $this->repository->getListOfMatches($app);
    }

    public function edit(Application $app, $id)
    {
        //
    }

    public function show(Application $app, $pair)
    {
       return $this->repository->getStatisticData($app, $pair);
    }

    public function store(Application $app,Request $request)
    {
        $this->data = json_decode($request->getContent(), true);

        return $this->repository->saveMessageToDB($app, $this->data);
    }

    public function update($id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
