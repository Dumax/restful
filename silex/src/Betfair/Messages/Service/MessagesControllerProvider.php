<?php
namespace Betfair\Messages\Service;

use Silex\Application;
use Silex\ControllerProviderInterface;

class MessagesControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $products = $app['controllers_factory'];

        $products->get("/", "Betfair\\Messages\\Controller\\MessagesController::index");

        $products->post("/", "Betfair\\Messages\\Controller\\MessagesController::store");

        $products->get("/{pair}", "Betfair\\Messages\\Controller\\MessagesController::show");

        $products->get("/edit/{id}", "Betfair\\Messages\\Controller\\MessagesController::edit");

        $products->put("/{id}", "Betfair\\Messages\\Controller\\MessagesController::update");

        $products->delete("/{id}", "Betfair\\Messages\\Controller\\MessagesController::destroy");

        return $products;
    }
}